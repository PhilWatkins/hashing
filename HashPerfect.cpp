#include <string>
#include <vector>
#include <iostream>
#include "HashPerfect.hpp"
using namespace std;

// CONSTRUCTOR FUNCTION
HashPerfect::HashPerfect()
{
    init(); // table size will always be 10
}

// PRINT FUCNTION
void HashPerfect::print()
{
    cout << "Perfect Hash Table: \n";

    for (int j = 0; j < 10; ++j)
    {
        cout << "  " << j << ": -->" << "\n";
        outterTable[j]->print();
    }
}

// INIT FUNCTION
void HashPerfect::init()
{
    for (int i = 0; i < 10; ++i)
        outterTable[i] = NULL;
}

// HASH FUNCTION
int HashPerfect::hash(string str) 
{
    unsigned long index = 0;
    for (int j = 0; j < str.length(); ++j)
        index = (37 * index + str[j]) % 10;
    return index % 10;
}

// INSERT FUCNTION
bool HashPerfect::insert(vector<string> keys)
{
    int array[10] = { 0 }; // array of 10 0's
    int inputSize = keys.size();

    // increment array value at hash index to count how many vals hash to each index
    for (int i = 0; i < inputSize; ++i)
        array[hash(keys[i])]++;

    // create HashLin objects of size (# inputs)^2
    for (int i = 0; i < 10; ++i)
        outterTable[i] = new HashLin(array[i] * array[i]); // must be deallocated later

    // go through all the strings being inserted and insert them
    for (int i = 0; i < inputSize; ++i)
    {
        int outerIndex = hash(keys[i]);
        outterTable[outerIndex]->insert(keys[i]);
    }

    print(); // done inserting, ready to print tables

    // deallocate memory used by HashLin objects
    for (int i = 0; i < 10; i++)
        delete outterTable[i];
    return true;
}
