#include <string>
#include <vector>
#include <iostream>
#include "HashLin.hpp"
using namespace std;

// INIT FUNCTION
void HashLin::init(int size) {
    // initializes all the values to ""
    for (int i = 0; i < table.size(); ++i)
        table[i] = "";
}

// CONSTRUCTOR FUNCTION
HashLin::HashLin(int s) {
    table = vector<string>(s);
    init(s); // call init to initialize the hash table of proper size
}

// INSERT FUNCTION
bool HashLin::insert(string value) 
{
    int index = hash(value); // index we are trying to insert at
    if (table[index] == "")
        table[index % table.size()] = value;
    else
    {
        while (table[index % table.size()] != "" ) // loop until a blank space is found
            index++;

        table[index % table.size()] = value;
    }
    return true;
}

// HASH FUNCTION
int HashLin::hash(string str)
{
    unsigned long index = 0; // unsigned to avoid negatives and whatnot
    for (int i = 0; i < str.length(); ++i)
        index = (37 * index + str[i]) % table.size();
    return index % table.size();
}

void HashLin::print()
{

    for (int i = 0; i < table.size(); ++i)
        cout << "        " << i << ": " << table[i] << "\n";
}
