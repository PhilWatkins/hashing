#ifndef HashPerfect_HPP // if HashPerfect not defined, define it
#define HashPerfect_HPP
#include <vector>
#include <string>
#include "HashLin.hpp"
using namespace std;

class HashPerfect
{
    private:
        HashLin *outterTable[10];
        void init();
        int hash(string);

    public:
        HashPerfect();
        bool insert(vector<string>);
        void print();
};

#endif // for defining HashPerfect