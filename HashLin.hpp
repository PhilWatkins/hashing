#ifndef HashLin_HPP // if HashLin not defined, define it
#define HashLin_HPP
#include <vector>
#include <string>
using namespace std;

class HashLin
{   
    private:
        vector<string> table;
        void init(int);
        int hash(string);

    public:
        HashLin(int size);
        bool insert(string);
        void print();
};

#endif // for defining HashLin