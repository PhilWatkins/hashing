#include <vector>
#include <iostream>
#include <string>
#include "HashLin.hpp"
#include "HashPerfect.hpp"
using namespace std;

int main()
{
    int primes[] = { 97,89,83,79,73,71,67,61,59,53,47,43,41,37,31,29,23,19,17,13,11,7,5,3,2 };
    string arr[97]; // assumes there wont be more than 97 values being hashed
    unsigned inputSize = sizeof(arr) / sizeof(arr[0]);
    int size;

    // read input file as done in assignment 2
    int i = 0;
    while(cin >> arr[i])
        ++i;

    arr[i] = ""; // reverts the processed "!" back to "" and then corrects the size (decrement by 1)
    i--;         // I wasn't sure how to make it stop reading the file when it got to "!"

    // make size the largest prime greater than number of arr being hashed
    int count = 0;
    while(i < primes[count])
        count++;
    size = primes[count-1];

    cout << "Please enter strings to insert (one per line; end list with '!'):\n\n";
    for (int n = 0; n < i; ++n)
        cout << arr[n] << "\n"; // print out the string inputs being hashed
    cout << "!\n\n\n";

    // HASHLIN
    HashLin table1 = HashLin(size);
    for (int k = 0; k < i; ++k)
        table1.insert(arr[k]); // insert each value
    cout << "Hash Table with Linear Probing (size = " << size << "): \n";
    table1.print();
    
    cout << "\n\n\n";

    // HASHPERFECT
    HashPerfect table2 = HashPerfect();
    vector<string> key;
    key.insert(key.end(), &arr[0], &arr[inputSize]);
    key.resize(i); // resize vector to minimum size needed
    table2.insert(key);

    return 0;
}